# Documentation

1. [API Gateway](apigateway.md)
2. [Abrechnung](billing.md)
3. [Compliance](compliance.md)
4. [Datenanalyse](data_analytics.md)
5. [Gesundheitsakten](health_records.md)
6. [Gesundheitsförderung in der Gemeinschaft](community_outreach.md)
7. [Gesundheitsversorgungsbildung](health_care_education.md)
8. [Gesundheitsversorgungsfinanzierung](health_care_financing.md)
9. [Gesundheitsversorgungsmanagement](health_care_management.md)
10. [Gesundheitsversorgungsplanung](health_care_planning.md)
11. [Gesundheitsversorgungspolitik](health_care_policy.md)
12. [Gesundheitsversorgungsforschung](health_care_research.md)
13. [Gesundheitskommunikation](health_communications.md)
14. [Gesundheitsdatenmanagement](health_data_management.md)
15. [Gesundheitsökonomie](health_economics.md)
16. [Gesundheitsbildung](health_education.md)
17. [Gesundheitsgerechtigkeit](health_equity.md)
18. [Gesundheitsinformationsmanagement](health_information_management.md)
19. [Gesundheitsinformations- und -kommunikationstechnologie](health_information_technology.md)
20. [Gesundheitsrecht](health_law.md)
21. [Gesundheitsmarketing](health_marketing.md)
22. [Gesundheitsmanagement](health_management.md)
23. [Gesundheitsförderung](health_promotion.md)
24. [Gesundheitsqualitätsmanagement](health_quality_management.md)
25. [Gesundheitsrisikomanagement](health_risk_management.md)
26. [Gesundheitsdienstleistungsforschung](health_services_research.md)
27. [Globale Gesundheit](global_health.md)
28. [Katastrophenvorsorge](disaster_response.md)
29. [Notfallmanagement](emergency_management.md)
30. [Patiententransport](patient_transportation.md)
31. [Pflegeplanung](care_planning.md)
32. [Psychische Gesundheit](mental_health.md)
33. [Telemedizin](telemedicine.md)
34. [Terminvergabe](appointment_scheduling.md)
35. [Umweltfreundliche Gesundheit](green_health.md)
36. [Umweltgesundheit](environmental_health.md)

## Name

*there is no name for the project yet. We will make a competition for that.

## Description

This documentation describes an open source hospitality system - the code is not yet public 

## Visuals

not yet - coming soon

## Installation

not yet - coming soon

## Usage

not yet - coming soon

## Support
 
not yet - coming soon

## Roadmap

we want to create a microservice architecture that is open for any kind of the listed modules 

## Contributing

We definatly need a community manager to define the contribution guidlines

## Authors and acknowledgment

Jochen Schultz <jschultz@php.net>

## License

This documentation is Licensed under WTFPL 

## Project status

we are prompting the system at the moment and working on getting as much data sources as possible 

